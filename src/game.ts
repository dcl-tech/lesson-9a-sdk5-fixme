/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'

const ground = spawnPlaneX (5,0,5,  90,0,0,  10,10,1)
const groundMaterial = new Material()
groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
ground.set(groundMaterial)

const box = spawnBoxX(5,1,5, 0,0,0,  1,1,1)

const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 3,1,3, 0,0,0, 1,1,1)

const naturePack = spawnGltfX(new GLTFShape("models/NaturePack/Nature-Pack-gltf-v1.3.gltf"), 7,0,5, 0,0,0,  1,1,1)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 2,0,2, 0,0,0,  .3,.3,.3)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5,1,2, 0,0,0, 0.05, 0.05, 0.05)



